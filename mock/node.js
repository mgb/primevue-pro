
const treetablenodes = require('./data/treetablenodes.json')
const treenodes = require('./data/treenodes.json')


module.exports = [
  {
    url: '/demo/data/treetablenodes.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  treetablenodes
      }
    }
  },
  {
    url: '/demo/data/treenodes.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  treenodes
      }
    }
  }
]
