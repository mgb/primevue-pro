
const products = require('./data/products.json')
const products_small = require('./data/products-small.json')
const products_orders_small = require('./data/products-orders-small.json')


module.exports = [
  {
    url: '/demo/data/products.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  products
      }
    }
  },
  {
    url: '/demo/data/products-small.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  products_small
      }
    }
  },
  {
    url: '/demo/data/products-orders-small.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  products_orders_small
      }
    }
  }
]
